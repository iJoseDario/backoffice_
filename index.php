<?php

    // Conexión BD
    $usuario = "backoffice_user"; $password = "N90#wmi8"; $servidor = "localhost"; $basededatos = "backoffice_angeles";
    $conexion = mysqli_connect( $servidor, $usuario, $password ) or die ("No se ha podido conectar al servidor de Base de datos");
    $db = mysqli_select_db( $conexion, $basededatos ) or die ( "Upps! Pues va a ser que no se ha podido conectar a la base de datos" );

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Historia </title>

    <!-- CSS -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="/assets/css/materialize.min.css" type="text/css" rel="stylesheet" media="screen,projection"/>
    <link href="/assets/css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
</head>
<body>

    <div class="container">
        <div class="row">
            <div class="col s12">

                <!-- Título -->
                <div class="row">
                    <div class="col s12">
                        <h1 class="center-align">Historia Clínica</h1>
                    </div>
                </div>

                <!-- Formulario: Datos básicos -->
                <div class="row">
                    <div class="col s12">
                        <h4>Datos Basicos</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s4">
                        <i class="material-icons prefix">account_circle</i>
                        <input id="dni" type="text" class="validate">
                        <label for="dni">DNI / Cédula</label>
                    </div>
                    <div class="input-field col s4">
                        <i class="material-icons prefix">account_circle</i>
                        <input id="nombre" type="text" class="validate">
                        <label for="nombre">Nombre (s)</label>
                    </div>
                    <div class="input-field col s4">
                        <i class="material-icons prefix">account_circle</i>
                        <input id="apellido" type="text" class="validate">
                        <label for="apellido">Apellidos</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s4">
                        <i class="material-icons prefix">date_range</i>
                        <input type="text" id="fecha--regisro" class="datepicker">
                        <label for="fecha--registro">Fecha registro</label>
                    </div>
                    <div class="input-field col s4">
                        <i class="material-icons prefix">phone</i>
                        <input id="celular" type="text" class="validate">
                        <label for="celular">Celular</label>
                    </div>
                    <div class="input-field col s4">
                        <i class="material-icons prefix">location_city</i>
                        <input id="ciudad--residencia" type="text" class="validate">
                        <label for="ciudad--residencia">Ciudad de residencia</label>
                    </div>
                </div>
                <div class="row">
                    <div class="col s12">
                        <label for="proceso">Proceso</label>
                        <p>
                            <label>
                                <input type="checkbox" name="proceso--exorcismo" id="proceso--exorcismo">
                                <span>Exorcismo</span>
                            </label>
                        </p>
                        <p>
                            <label>
                                <input type="checkbox" name="proceso--liberacion" id="proceso--liberacion">
                                <span>Liberación</span>
                            </label>
                        </p>
                        <p>
                            <label>
                                <input type="checkbox" name="proceso--reiki" id="proceso--reiki">
                                <span>Sanación y Liberación REIKI</span>
                            </label>
                        </p>
                        <p>
                            <label>
                                <input type="checkbox" name="proceso--ataduras" id="proceso--ataduras">
                                <span>Rompimiento Ataduras Espirituales</span>
                            </label>
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s6">
                        <textarea id="diagnostico" class="materialize-textarea" rows="3"></textarea>
                        <label for="diagnostico">Diagnóstico Discernimiento Espiritual</label>
                    </div>
                    <div class="input-field col s6">
                        <textarea id="causas" class="materialize-textarea" rows="3"></textarea>
                        <label for="causas">Causas y Síntomas</label>
                    </div>
                </div>

                <!-- Ocultismo -->
                <div class="row">
                    <div class="col s12">
                        <h4>Etapa 1: Ocultismo</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col s12">
                        <table class="striped">
                            <thead>
                                <tr>
                                    <th>Item</th>
                                    <th class="center">Nunca</th>
                                    <th class="center">Aveces</th>
                                    <th class="center">Moderadamente</th>
                                    <th class="center">Bastante</th>
                                    <th class="center">Mucho</th>
                                </tr>
                            </thead>

                            <tbody>
                                <?php
                                    $consulta = "SELECT * FROM itemsOcultismo";
                                    $resultado = mysqli_query( $conexion, $consulta ) or die ( "Algo ha ido mal en la consulta a la base de datos");

                                    while ($columna = mysqli_fetch_array( $resultado )) { ?>
                                        <tr>
                                            <td><?php echo $columna['item']; ?></td>
                                            <td class="center">
                                                <p>
                                                    <label>
                                                        <input type="radio" name="group<?php echo $columna['id']; ?>" value="ocultismo--item<?php echo $columna['id']; ?>--nunca">
                                                        <span></span>
                                                    </label>
                                                </p>
                                            </td>
                                            <td class="center">
                                                <p>
                                                    <label>
                                                        <input type="radio" name="group<?php echo $columna['id']; ?>" value="ocultismo--item<?php echo $columna['id']; ?>--aveces">
                                                        <span></span>
                                                    </label>
                                                </p>
                                            </td>
                                            <td class="center">
                                                <p>
                                                    <label>
                                                        <input type="radio" name="group<?php echo $columna['id']; ?>" value="ocultismo--item<?php echo $columna['id']; ?>--moderadamente">
                                                        <span></span>
                                                    </label>
                                                </p>
                                            </td>
                                            <td class="center">
                                                <p>
                                                    <label>
                                                        <input type="radio" name="group<?php echo $columna['id']; ?>" value="ocultismo--item<?php echo $columna['id']; ?>--bastante">
                                                        <span></span>
                                                    </label>
                                                </p>
                                            </td>
                                            <td class="center">
                                                <p>
                                                    <label>
                                                        <input type="radio" name="group<?php echo $columna['id']; ?>" value="ocultismo--item<?php echo $columna['id']; ?>--mucho">
                                                        <span></span>
                                                    </label>
                                                </p>
                                            </td>
                                        </tr>
                                    <?php }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <textarea id="ocultismo--otros" class="materialize-textarea" rows="3"></textarea>
                        <label for="ocultismo--otros">Otras actividades</label>
                    </div>
                </div>

                <!-- Heridas del Alma -->
                <div class="row">
                    <div class="col s12">
                        <h4>Etapa 2: Heridas del Alma</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col s12">
                        <table class="striped">
                            <thead>
                                <tr>
                                    <th>Item</th>
                                    <th class="center">Seleccione</th>
                                    <th class="center">Nada</th>
                                    <th class="center">Poco</th>
                                    <th class="center">Normal</th>
                                    <th class="center">Bastante</th>
                                    <th class="center">Mucho</th>
                                </tr>
                            </thead>

                            <tbody>
                                <?php
                                    $consulta = "SELECT * FROM itemsHeridas";
                                    $resultado = mysqli_query( $conexion, $consulta ) or die ( "Algo ha ido mal en la consulta a la base de datos");

                                    while ($columna = mysqli_fetch_array( $resultado )) { ?>
                                        <tr>
                                            <td><?php echo $columna['item']; ?></td>
                                            <td>
                                                <select>
                                                    <option value="" disabled selected>Elige tu opción</option>
                                                    <option value="1">Option 1</option>
                                                    <option value="2">Option 2</option>
                                                    <option value="3">Option 3</option>
                                                </select>
                                            </td>
                                            <td class="center">
                                                <p>
                                                    <label>
                                                        <input type="radio" name="group<?php echo $columna['id']; ?>" id="">
                                                        <span></span>
                                                    </label>
                                                </p>
                                            </td>
                                            <td class="center">
                                                <p>
                                                    <label>
                                                        <input type="radio" name="group<?php echo $columna['id']; ?>" id="">
                                                        <span></span>
                                                    </label>
                                                </p>
                                            </td>
                                            <td class="center">
                                                <p>
                                                    <label>
                                                        <input type="radio" name="group<?php echo $columna['id']; ?>" id="">
                                                        <span></span>
                                                    </label>
                                                </p>
                                            </td>
                                            <td class="center">
                                                <p>
                                                    <label>
                                                        <input type="radio" name="group<?php echo $columna['id']; ?>" id="">
                                                        <span></span>
                                                    </label>
                                                </p>
                                            </td>
                                            <td class="center">
                                                <p>
                                                    <label>
                                                        <input type="radio" name="group<?php echo $columna['id']; ?>" id="">
                                                        <span></span>
                                                    </label>
                                                </p>
                                            </td>
                                        </tr>
                                    <?php }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <textarea id="heridas--otros" class="materialize-textarea" rows="3"></textarea>
                        <label for="heridas--otros">Observaciones</label>
                    </div>
                </div>

                <!-- Conduta moral -->
                <div class="row">
                    <div class="col s12">
                        <h4>Etapa 3: Conduta moral</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col s12">
                        <table class="striped">
                            <thead>
                                <tr>
                                    <th>Item</th>
                                    <th class="center">Nunca</th>
                                    <th class="center">Aveces</th>
                                    <th class="center">Moderadamente</th>
                                    <th class="center">Bastante</th>
                                    <th class="center">Mucho</th>
                                </tr>
                            </thead>

                            <tbody>
                                <?php
                                    $consulta = "SELECT * FROM itemsConducta";
                                    $resultado = mysqli_query( $conexion, $consulta ) or die ( "Algo ha ido mal en la consulta a la base de datos");

                                    while ($columna = mysqli_fetch_array( $resultado )) { ?>
                                        <tr>
                                            <td><?php echo $columna['item']; ?></td>
                                            <td class="center">
                                                <p>
                                                    <label>
                                                        <input type="radio" name="group<?php echo $columna['id']; ?>" id="">
                                                        <span></span>
                                                    </label>
                                                </p>
                                            </td>
                                            <td class="center">
                                                <p>
                                                    <label>
                                                        <input type="radio" name="group<?php echo $columna['id']; ?>" id="">
                                                        <span></span>
                                                    </label>
                                                </p>
                                            </td>
                                            <td class="center">
                                                <p>
                                                    <label>
                                                        <input type="radio" name="group<?php echo $columna['id']; ?>" id="">
                                                        <span></span>
                                                    </label>
                                                </p>
                                            </td>
                                            <td class="center">
                                                <p>
                                                    <label>
                                                        <input type="radio" name="group<?php echo $columna['id']; ?>" id="">
                                                        <span></span>
                                                    </label>
                                                </p>
                                            </td>
                                            <td class="center">
                                                <p>
                                                    <label>
                                                        <input type="radio" name="group<?php echo $columna['id']; ?>" id="">
                                                        <span></span>
                                                    </label>
                                                </p>
                                            </td>
                                        </tr>
                                    <?php }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col s6">
                        <label for="proceso">Eres adicto a:</label>
                        <p>
                            <label>
                                <input type="checkbox" name="proceso--exorcismo" id="proceso--exorcismo">
                                <span>Juegos?</span>
                            </label>
                        </p>
                        <p>
                            <label>
                                <input type="checkbox" name="proceso--exorcismo" id="proceso--exorcismo">
                                <span>Sexo?</span>
                            </label>
                        </p>
                        <p>
                            <label>
                                <input type="checkbox" name="proceso--exorcismo" id="proceso--exorcismo">
                                <span>Drogas?</span>
                            </label>
                        </p>
                        <p>
                            <label>
                                <input type="checkbox" name="proceso--exorcismo" id="proceso--exorcismo">
                                <span>Pornografia?</span>
                            </label>
                        </p>
                        <p>
                            <label>
                                <input type="checkbox" name="proceso--exorcismo" id="proceso--exorcismo">
                                <span>Masturbación?</span>
                            </label>
                        </p>
                        <p>
                            <label>
                                <input type="checkbox" name="proceso--exorcismo" id="proceso--exorcismo">
                                <span>Culto al cuerpo?</span>
                            </label>
                        </p>
                        <p>
                            <label>
                                <input type="checkbox" name="proceso--exorcismo" id="proceso--exorcismo">
                                <span>Comprar cosas?</span>
                            </label>
                        </p>
                        <p>
                            <label>
                                <input type="checkbox" name="proceso--exorcismo" id="proceso--exorcismo">
                                <span>Comidas?</span>
                            </label>
                        </p>
                    </div>
                    <div class="col s6">
                        <label for="proceso">Sufriste alguna vez de:</label>
                        <p>
                            <label>
                                <input type="checkbox" name="proceso--exorcismo" id="proceso--exorcismo">
                                <span>Bulimia?</span>
                            </label>
                        </p>
                        <p>
                            <label>
                                <input type="checkbox" name="proceso--exorcismo" id="proceso--exorcismo">
                                <span>Anorexia?</span>
                            </label>
                        </p>
                        <p>
                            <div class="input-field">
                                <input id="enfermedad--otra" type="text" class="validate">
                                <label for="enfermedad--otra">Otros, cúal?</label>
                            </div>
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <textarea id="conducta--observacione" class="materialize-textarea" rows="3"></textarea>
                        <label for="conducta--observacione">Observaciones</label>
                    </div>
                </div>

                <!-- Historia Familiar -->
                <div class="row">
                    <div class="col s12">
                        <h4>Historia Familiar</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col s12">
                        <table class="striped">
                            <thead>
                                <tr>
                                    <th>Item</th>
                                    <th class="center">Papá</th>
                                    <th class="center">Mamá</th>
                                    <th class="center">Abuelos</th>
                                </tr>
                            </thead>

                            <tbody>
                                <tr>
                                    <td>A quien te pareces fisicamente?</td>
                                    <td class="center">
                                        <p>
                                            <label>
                                                <input name="group-1255" type="radio" />
                                                <span></span>
                                            </label>
                                        </p>
                                    </td>
                                    <td class="center">
                                        <p>
                                            <label>
                                                <input name="group-1255" type="radio" />
                                                <span></span>
                                            </label>
                                        </p>
                                    </td>
                                    <td class="center">
                                        <p>
                                            <label>
                                                <input name="group-1255" type="radio" />
                                                <span></span>
                                            </label>
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>A quien te pareces emocionalmente?</td>
                                    <td class="center">
                                        <p>
                                            <label>
                                                <input name="group-1256" type="radio" />
                                                <span></span>
                                            </label>
                                        </p>
                                    </td>
                                    <td class="center">
                                        <p>
                                            <label>
                                                <input name="group-1256" type="radio" />
                                                <span></span>
                                            </label>
                                        </p>
                                    </td>
                                    <td class="center">
                                        <p>
                                            <label>
                                                <input name="group-1256" type="radio" />
                                                <span></span>
                                            </label>
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>A quien te pareces intelectualmente?</td>
                                    <td class="center">
                                        <p>
                                            <label>
                                                <input name="group-1257" type="radio" />
                                                <span></span>
                                            </label>
                                        </p>
                                    </td>
                                    <td class="center">
                                        <p>
                                            <label>
                                                <input name="group-1257" type="radio" />
                                                <span></span>
                                            </label>
                                        </p>
                                    </td>
                                    <td class="center">
                                        <p>
                                            <label>
                                                <input name="group-1257" type="radio" />
                                                <span></span>
                                            </label>
                                        </p>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s6">
                        <input id="repetir--enfermedad" type="text" class="validate">
                        <label for="repetir--enfermedad">Repites alguna enfermedad? De quien?</label>
                    </div>
                    <div class="input-field col s6">
                        <input id="repetir--actitudes" type="text" class="validate">
                        <label for="repetir--actitudes">Repites actitudes violentas? De quien?</label>
                    </div>
                </div>
                <div class="row">
                    <div class="col s12">
                        <label for="repetir--vicios">Repites vicios?</label>
                        <p>
                            <label>
                                <input type="checkbox" name="rv--alcohol" id="">
                                <span>Alcohol</span>
                            </label>
                        </p>
                        <p>
                            <label>
                                <input type="checkbox" name="proceso--exorcismo" id="proceso--exorcismo">
                                <span>Cigarros</span>
                            </label>
                        </p>
                        <p>
                            <label>
                                <input type="checkbox" name="proceso--exorcismo" id="proceso--exorcismo">
                                <span>Drogas</span>
                            </label>
                        </p>
                        <p>
                            <label>
                                <input type="checkbox" name="proceso--exorcismo" id="proceso--exorcismo">
                                <span>Gula</span>
                            </label>
                        </p>
                        <p>
                            <div class="input-field">
                                <input id="repetir--vicios--otra" type="text" class="validate">
                                <label for="repetir--vicios--otra">Otros, cúal?</label>
                            </div>
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s6">
                        <input id="repetir--enfermedad" type="text" class="validate">
                        <label for="repetir--enfermedad">Hubo suicidios o muertes tragicas en la familia? De quien?</label>
                    </div>
                    <div class="input-field col s6">
                        <input id="repetir--enfermedad" type="text" class="validate">
                        <label for="repetir--enfermedad">Hubo abortos en la familia? De quien?</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s6">
                        <input id="repetir--enfermedad" type="text" class="validate">
                        <label for="repetir--enfermedad">Fuiste hijo/a deseado/a?</label>
                    </div>
                    <div class="input-field col s6">
                        <input id="repetir--enfermedad" type="text" class="validate">
                        <label for="repetir--enfermedad">Practicas algun ritual de tus padres o abuelos?</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s6">
                        <input id="repetir--enfermedad" type="text" class="validate">
                        <label for="repetir--enfermedad">Guardas algun amuleto o regalo con alguna promesa? De quien?</label>
                    </div>
                    <div class="input-field col s6">
                        <input id="repetir--enfermedad" type="text" class="validate">
                        <label for="repetir--enfermedad">Hubo deudas, escasez, miserias o limitaciones??</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s6">
                        <input id="repetir--enfermedad" type="text" class="validate">
                        <label for="repetir--enfermedad">Viven o vivieron en asa propia o alquilada?</label>
                    </div>
                    <div class="input-field col s6">
                        <input id="repetir--enfermedad" type="text" class="validate">
                        <label for="repetir--enfermedad">Eres autosuficientes o dependiente?</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <select name="parecido--intelectual" id="parecido--intelectual">
                            <option value="" disabled selected>Elige tu opción</option>
                            <option value="1">Si</option>
                            <option value="2">No</option>
                        </select>
                        <label>Estas listo/a para renunciar a los aspectos negativos que causaron y/o causan sufrimiento en tu historia pasada?</label>
                    </div>
                </div>

                <!-- Observaciones Generales -->
                <div class="row">
                    <div class="col s12">
                        <h4>Observaciones Generales</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <textarea id="ocultismo--otros" class="materialize-textarea" rows="3"></textarea>
                        <label for="ocultismo--otros">Observaciones</label>
                    </div>
                </div>

                <!-- Guardar -->
                <div class="fixed-action-btn">
                    <a class="btn-floating btn-large">
                        <i class="large material-icons">menu</i>
                    </a>
                    <ul>
                        <li><a class="btn-floating red"><i class="material-icons">save</i></a></li>
                        <li><a class="btn-floating yellow darken-1"><i class="material-icons">search</i></a></li>
                        <li><a class="btn-floating green"><i class="material-icons">publish</i></a></li>
                        <li><a class="btn-floating blue"><i class="material-icons">attach_file</i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <!--  Scripts-->
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script src="/assets/js/materialize.min.js"></script>
    <script src="/assets/js/script.js"></script>
</body>
</html>